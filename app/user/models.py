from pydantic import BaseModel


class User(BaseModel):
    user_id: str
    name: str
    age: int
    gender: str
    occupation: str
    movie_history: dict


class UserInDB(User):
    hashed_password: str


class UserInCreate(User):
    password: str
