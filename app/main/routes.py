from flask import Flask, request, jsonify, make_response
from flask_jwt_extended import JWTManager, create_access_token, get_jwt_identity
from app.movie.models import Movie
from app.user.models import User
from crud import create_user, get_user, get_users, authenticate_user, create_movie, get_movies, get_movie

app = Flask(__name__)
app.config['JWT_SECRET_KEY'] = 'secret-key'
jwt = JWTManager(app)


@app.route('/users', methods=['POST'])
def create_user_route():
    data = request.get_json()
    user = User(**data)
    created = create_user(user)
    if created:
        return jsonify({'message': 'User created successfully'}), 201
    return jsonify({'message': 'User already exists'}), 400


@app.route('/users', methods=['GET'])
def read_users_route():
    users = get_users()
    return jsonify({'users': [user.to_dict() for user in users]})


@app.route('/users/<user_id>', methods=['GET'])
def read_user_route(user_id):
    user = get_user(user_id)
    if user:
        return jsonify({'user': user.to_dict()})
    return jsonify({'message': 'User not found'}), 404


@app.route('/login', methods=['POST'])
def login_route():
    data = request.get_json()
    username = data.get('username')
    password = data.get('password')
    user = authenticate_user(username, password)
    if user:
        access_token = create_access_token(identity=user.user_id)
        return jsonify({'access_token': access_token}), 200
    return jsonify({'message': 'Invalid credentials'}), 400


@app.route('/movies', methods=['POST'])
@jwt
def create_movie_route():
    data = request.get_json()
    movie = Movie(**data)
    created = create_movie(movie)
    if created:
        return jsonify({'message': 'Movie created successfully'}), 201
    return jsonify({'message': 'Movie already exists'}), 400


@app.route('/movies', methods=['GET'])
def read_movies_route():
    movies = get_movies()
    return jsonify({'movies': [movie.to_dict() for movie in movies]})


@app.route('/movies/<movie_id>', methods=['GET'])
def read_movie_route(movie_id):
    movie = get_movie(movie_id)
    if movie:
        return jsonify({'movie': movie.to_dict()})
    return jsonify({'message': 'Movie not found'}), 404
