from pydantic import BaseModel


class Movie(BaseModel):
    movie_id: str
    title: str
    genre: str
    cast: list
    director: str
    release_date: str
    popularity_score: float


class MovieInDB(Movie):
    pass


class MovieInCreate(Movie):
    pass
