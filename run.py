#from celery.app import Celery
# from celery.contrib.pytest import Celery
from flask import Flask
from celery import Celery
from flask_marshmallow import Marshmallow
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config.from_pyfile('config.py')

celery = Celery(app.name, broker=app.config['CELERY_BROKER_URL'])
celery.conf.update(app.config)

db = SQLAlchemy(app)
ma = Marshmallow(app)

if __name__ == '__main__':
    app.run()
