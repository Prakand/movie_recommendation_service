from gremlin_python import statics
from gremlin_python.structure.graph import Graph
from gremlin_python.process.graph_traversal import __

statics.load_statics(globals())


class GraphOperations:
    def __init__(self, endpoint):
        self.graph = Graph().traversal().withRemote(endpoint)

    def add_movie(self, movie_id, title, genre, cast, director, release_date):
        self.graph.addV('movie').property('movie_id', movie_id).property('title', title).property('genre',
                                                                                                  genre).property(
            'cast', cast).property('director', director).property('release_date', release_date).next()

    def add_user(self, user_id, username):
        self.graph.addV('user').property('user_id', user_id).property('username', username).next()

    def add_rating(self, user_id, movie_id, rating):
        self.graph.V().has('user', 'user_id', user_id).as_('u').V().has('movie', 'movie_id', movie_id).as_('m').addE(
            'rated').from_('u').to('m').property('rating', rating).next()

    def get_movie(self, movie_id):
        return self.graph.V().has('movie', 'movie_id', movie_id).valueMap(True).next()

    def get_user(self, user_id):
        return self.graph.V().has('user', 'user_id', user_id).valueMap(True).next()

    def get_movies_by_genre(self, genre):
        return self.graph.V().has('movie', 'genre', genre).valueMap(True).toList()

    def get_movies_by_cast(self, cast):
        return self.graph.V().has('movie', 'cast', cast).valueMap(True).toList()

    def get_movies_by_director(self, director):
        return self.graph.V().has('movie', 'director', director).valueMap(True).toList()

    def get_user_ratings(self, user_id):
        return self.graph.V().has('user', 'user_id', user_id).outE('rated').valueMap(True).toList()

    def get_movie_ratings(self, movie_id):
        return self.graph.V().has('movie', 'movie_id', movie_id).inE('rated').valueMap(True).toList()

    def get_similar_movies(self, movie_id):
        return self.graph.V().has('movie', 'movie_id', movie_id).out().dedup().valueMap(True).toList()
