import unittest
import requests
import json

BASE_URL = 'http://localhost:5000/'


class TestMovieRecommendationService(unittest.TestCase):
    def setUp(self):
        # Create a user
        self.user_data = {
            'username': 'testuser',
            'password': 'password'
        }
        response = requests.post(BASE_URL + 'users', json=self.user_data)
        self.assertEqual(response.status_code, 201)

        # Log in the user and get access token
        response = requests.post(BASE_URL + 'login', json=self.user_data)
        self.assertEqual(response.status_code, 200)
        self.access_token = response.json()['access_token']

    def test_create_movie(self):
        # Test create movie without authorization
        movie_data = {
            'title': 'Test Movie',
            'genre': 'Action',
            'cast': ['Actor 1', 'Actor 2'],
            'director': 'Test Director',
            'release_date': '2022-01-01'
        }
        response = requests.post(BASE_URL + 'movies', json=movie_data)
        self.assertEqual(response.status_code, 401)

        # Test create movie with authorization
        headers = {
            'Authorization': 'Bearer ' + self.access_token
        }
        response = requests.post(BASE_URL + 'movies', json=movie_data, headers=headers)
        self.assertEqual(response.status_code, 201)

    def test_read_movies(self):
        # Test read all movies
        response = requests.get(BASE_URL + 'movies')
        self.assertEqual(response.status_code, 200)
        self.assertIsInstance(response.json()['movies'], list)

    def test_read_movie(self):
        # Test read movie not found
        response = requests.get(BASE_URL + 'movies/100')
        self.assertEqual(response.status_code, 404)

        # Test read movie found
        movie_data = {
            'title': 'Test Movie 2',
            'genre': 'Comedy',
            'cast': ['Actor 3', 'Actor 4'],
            'director': 'Test Director 2',
            'release_date': '2022-01-02'
        }
        headers = {
            'Authorization': 'Bearer ' + self.access_token
        }
        response = requests.post(BASE_URL + 'movies', json=movie_data, headers=headers)
        movie_id = response.json()['movie_id']
        response = requests.get(BASE_URL + 'movies/' + str(movie_id))
        self.assertEqual(response.status_code, 200)


if __name__ == '__main__':
    pass
